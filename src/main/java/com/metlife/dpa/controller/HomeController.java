package com.metlife.dpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.metlife.dpa.model.Greeting;
import com.metlife.dpa.model.dogGreeting;

@Controller
public class HomeController {

	@RequestMapping(value="/")
	public String sayHello(Model model){
		model.addAttribute("greeting", "Hello World!");
		return "index";
	}
	
    @RequestMapping(value="/greeting", method=RequestMethod.GET)
    public String greetingForm(Model model) {
    	Greeting greet = new Greeting();
    	greet.setContent("Set from the controller");
        model.addAttribute("formGreeting", greet);
        return "greeting";
    }

    @RequestMapping(value="/greeting", method=RequestMethod.POST)
    public String greetingSubmit(@ModelAttribute Greeting greeting, Model model) {
    	System.out.println(greeting.getContent());
        model.addAttribute("greeting", greeting);
        return "result";
    }
    @RequestMapping(value="/dog", method=RequestMethod.GET)
    public String dogForm(Model model) {
    	dogGreeting dogGreeting = new dogGreeting();
        model.addAttribute("dogFormGreeting", dogGreeting);
        return "dogGreeting";
    }

    @RequestMapping(value="/dog", method=RequestMethod.POST)
    public String dogSubmit(@ModelAttribute dogGreeting dogGreeting, Model model) {
    	System.out.println(dogGreeting.getContent());
        model.addAttribute("dogGreeting", dogGreeting);
        return "result";
    }
	
}
