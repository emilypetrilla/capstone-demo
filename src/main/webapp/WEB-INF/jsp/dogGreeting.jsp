<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Doggy Form, Whaddup</title>
<link rel="shortcut icon" href="">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
<style>
body {
	padding-top: 50px;
}

.starter-template {
	padding: 40px 15px;
	text-align: center;
}
</style>

<!--[if IE]>
        <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	

	<div class="container">
		<div class="starter-template">
		<h1><i>Cooler</i> Dog Form</h1>
		<h3>(Than a <i>cat</i> form...)</h3>
			<p class="lead">
			
			</p>	
		</div>
		<form:form commandName="dogFormGreeting">
			<div class="form-group">
				<form:label path="name" for="dogName">Name</form:label> 
				<form:input path="name" type="text"
					class="form-control" id="dogName" placeholder="Enter dog name"/>
				
			</div>
			<div class="form-group">
				<form:label path="age"  for="dogAge">Age</form:label> 
				<form:input path="age" type="number"
					class="form-control" id="dogAge" placeholder="Enter dog age"/>
			</div>
			<div class="form-group">
				<form:label path="breed" for="dogBreed">Breed</form:label> 
				<form:input path="breed" type="text"
					class="form-control" id="dogBreed" placeholder="Enter dog breed"/>
			</div>
			<form:button type="submit" class="btn btn-default">Submit</form:button>
		</form:form>
	</div>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>



